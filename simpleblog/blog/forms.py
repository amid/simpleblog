# coding: utf8
from django import forms
from .models import Post, Comment
from django.forms import ModelForm


class CreateFormPost(forms.ModelForm):
    class Meta(object):
        model = Post

        def __init__(self, *args, **kwargs):
            super(CreateFormPost, self).__init__(*args, **kwargs)
            self.fields['object_delete'].widget = forms.HiddenInput()


class CreateFormComment(ModelForm):
    class Meta:
        model = Comment

    def __init__(self, *args, **kwargs):
        super(CreateFormComment, self).__init__(*args, **kwargs)
        self.fields['object_delete'].widget = forms.HiddenInput()
        self.fields['post'].widget = forms.HiddenInput()
