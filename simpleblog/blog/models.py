# coding: utf8

from django.db import models
from django.core.validators import MaxLengthValidator, MinLengthValidator


class Category(models.Model):
    name = models.CharField(max_length=255)
    object_delete = models.BooleanField(verbose_name=u'Отметка об удалении',
                                        default=False)

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100)
    object_delete = models.BooleanField(verbose_name=u'Отметка об удалении',
                                        default=False)

    def __unicode__(self):
        return "%s" % self.name


class Post(models.Model):
    date_created = models.DateTimeField(u'Дата публикации')
    title = models.CharField(max_length=255)
    content = models.TextField(max_length=10000)
    tags = models.ManyToManyField(Tag)
    object_delete = models.BooleanField(verbose_name=u'Отметка об удалении',
                                        default=False)

    def __unicode__(self):
        return unicode("%s" % self.title)

    def get_absolute_url(self):
        return "/blog/%i/" % self.id


class Comment(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=150, verbose_name=u'Автор',
                              validators=[MaxLengthValidator(50),
                                          MinLengthValidator(2)]
                              )
    content = models.TextField(max_length=255, verbose_name=u'Содержание',
                               validators=[MaxLengthValidator(255)]
                               )
    post = models.ForeignKey(Post)
    object_delete = models.BooleanField(verbose_name=u'Отметка об удалении',
                                        default=False)

    def __unicode__(self):
        return unicode("%s: %s" % (self.author,  self.content))

    def short(self):
        return unicode("%s" % (self.content[:10]))
