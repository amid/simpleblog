# coding: utf8

from .models import Post, Tag, Comment
from .forms import CreateFormPost, CreateFormComment
from django.views.generic import CreateView, ListView, DetailView, FormView
from django.http import Http404
from django.http import HttpResponseRedirect
from django.contrib import messages


class PostView(ListView):
    # список всех постов
    model = Post
    template_name = 'base.html'
    context_object_name = 'post_list'
    paginate_by = 5

    def get_queryset(self):
        query = super(PostView, self).get_queryset().filter(
            object_delete=False).order_by('-date_created')
        return query

    def get_context_data(self, **kwargs):
        context_data = super(PostView, self).get_context_data(**kwargs)
        query_tag = Tag.objects.all().order_by('name')
        context_data['tag_list'] = query_tag
        return context_data


class CreatePost(CreateView):
    # создание поста
    template_name = 'create_post.html'
    model = Post
    form_class = CreateFormPost
    success_url = '/'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            self.object = None
            form_class = self.get_form_class()
            form = self.get_form(form_class)
            print form
        else:
            raise Http404
        return self.render_to_response(
            self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        form.save_m2m()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(
            self.get_context_data(form=form))


class ViewPost(DetailView):
    # просмотр поста
    model = Post
    template_name = 'detail_post.html'

    def get_context_data(self, **kwargs):
        context = super(ViewPost, self).get_context_data(**kwargs)
        query_comments = Comment.objects.filter(post=self.object.id
                                                ).order_by('-date_created')
        context['view_comments'] = query_comments
        context['form'] = CreateFormComment(initial={'post': self.object.id})
        return context


class CommentCreate(FormView):
    # создание комментария к посту
    form_class = CreateFormComment
    template_name = 'detail_post.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS,
                             'Ваш комментарий опубликован')
        return self.request.META.get('HTTP_REFERER', None)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class ListPostByTag(DetailView):
    # список статей по тегам
    model = Tag
    template_name = 'search.html'
    slug_field = 'name'

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated():
            context_data = super(ListPostByTag, self
                                 ).get_context_data(**kwargs)
            obj = self.get_object()
            post_by_tag = Post.objects.filter(tags__id=obj.id)
            context_data['list_post'] = post_by_tag
            return context_data
        else:
            return HttpResponseRedirect('/')
