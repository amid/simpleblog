# coding: utf8
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.decorators.http import require_POST
from django.contrib import admin
from django.conf import settings
admin.autodiscover()
from blog.views import CreatePost, PostView, ViewPost, CommentCreate, ListPostByTag

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^create_post/$', CreatePost.as_view()),
                       url(r'^$', PostView.as_view()),
                       url(r'^view_post/(?P<pk>\d{1,9})/$', ViewPost.as_view()),
                       url('^create_comment/$',
                           require_POST(CommentCreate.as_view()),
                           name='create_comment_view_url'),
                       url(r'^post_by_tag/(?P<slug>\w+)/$',
                           ListPostByTag.as_view(), name='post_by_tag'),
                       )


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
