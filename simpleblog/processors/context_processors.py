# coding: utf8

from django.conf import settings
from simpleblog.blog.models import Tag

def utils(request):
    return {
        'title': 'Мой блог',
        'my_settings': settings,
        'tag_list': Tag.objects.all().order_by('name')
    }
